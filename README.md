# ContactsDB NodeJs App

this project is used as the backend of a Contacts application
Please run the database.sql script to create the database and some previously saved contacts.

do npm run devStart to run after the database is created.

Database connection information:

const db = mysql.createPool({
    host: 'localhost',
    user:  'root',
    database: 'contactsbd'
})

see index.js file