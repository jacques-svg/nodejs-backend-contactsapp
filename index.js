const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const app = express()

const mysql = require('mysql')


//Connexion to the database
const db = mysql.createPool({
    host: 'localhost',
    user:  'root',
    database: 'contactsbd'
})

app.use(cors({}));
app.use(express.json());
app.use(bodyParser.urlencoded({extended: true}));

//GET ALL THE Contacts FROM THE DB
app.get('/api/contacts', (req,res)=>{
    const sqlSelect = "SELECT * FROM contacts;"

    db.query(sqlSelect, (err, rows)=>{
       res.send(rows); // send the data in the API to the FrontEnd
    })

})

//Insert a contact in the database
app.post("/api/insert", (req,res)=>{

     const {firstName,lastName,email,phone} = req.body
 
     const sqlInsert = "INSERT INTO contacts (firstName,lastName,email,phone) VALUES (?,?,?,?)"
     //Requete
     db.query(sqlInsert,[firstName,lastName,email,phone], (err, result)=>{

        if(err) {
            console.log(err)
            res.send("Insert failed!")
        }
        else {
            console.log(result);
        }

     }) 
 
 });

 //Update a specific contact in the database
 app.put('/api/update', (req,res)=>{
    
    const {id,firstName,lastName,email,phone} = req.body
    
    const sqlUpdate = "UPDATE contacts SET firstName = ?,lastName = ?,email = ?,phone = ? WHERE id = ?";
    //Request
    db.query(sqlUpdate,[firstName,lastName,email,phone,id], (err, result)=>{
      if(err) console.log(err)
      else{
          res.send("Update Success!")
      }
    })
})

 //DELETE a contact in the database
 app.delete('/api/delete/:userId', (req,res)=>{
    
    const {userId} = req.params;
    const sqlDelete = "DELETE FROM contacts WHERE id = ?";
    db.query(sqlDelete,userId, (err, result)=>{

      if(err) console.log(err)
      else{
        res.send('Delete Success!')
    }
    })
})

//Get the current contact in the database for update
app.get('/api/current/user/:id', (req,res)=>{
    
    const {id} = req.params;
    //console.log('idCu',id);

    const sqlGet = "SELECT * FROM contacts WHERE id = ?";
    //Request
    db.query(sqlGet,id, (err, result)=>{
      if(err) console.log(err)
      else{
          res.send(result)
      }
    })
    
})

app.listen(3001, () => {
    console.log("running on port 3001");
})